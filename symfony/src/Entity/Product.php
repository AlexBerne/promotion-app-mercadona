<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use App\Controller\ProductImageController;
use App\Controller\ProductPatchController;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[Vich\Uploadable]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(
            controller: ProductImageController::class,
            deserialize: false,
            security: "is_granted('IS_AUTHENTICATED_FULLY')",
        ),
        new Patch(
            controller: ProductPatchController::class,
            deserialize: false,
            security: "is_granted('IS_AUTHENTICATED_FULLY')",
        ),
        new Delete(
            security: "is_granted('IS_AUTHENTICATED_FULLY')",
        )
    ],
    normalizationContext: [
        'groups' => ['product'],
        "enable_max_depth" => true,
    ],
)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['product'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['product'])]
    private ?string $label = null;

    #[ORM\Column(length: 2000, nullable: true)]
    #[Groups(['product'])]
    private ?string $description = null;

    #[ORM\Column]
    #[Groups(['product'])]
    private ?float $price = null;

    #[ApiProperty(types: ['https://schema.org/contentUrl'])]
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['product'])]
    private ?string $image = null;

    // /*
    //  * @Vich\UploadableField(mapping="media_object", fileNameProperty="image")
    //  */
    #[Vich\UploadableField(mapping: 'media_object', fileNameProperty: 'image')]
    private ?File $file = null;

    #[ORM\ManyToOne(
        inversedBy: 'products',
        cascade: ['persist'],
        targetEntity: Category::class,
    )]
    #[Groups(['product'])]
    private ?Category $category = null;

    #[ORM\OneToOne(inversedBy: 'product',
        cascade: ['persist', 'remove'], )]
    #[MaxDepth(2)]
    #[Groups(['product'])]
    private ?Promotion $promotion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?file $file): self
    {
        $this->file = $file;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPromotion(): ?Promotion
    {
        return $this->promotion;
    }

    public function setPromotion(?Promotion $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }
}
