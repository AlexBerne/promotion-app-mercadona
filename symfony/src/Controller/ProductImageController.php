<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Promotion;
use App\Repository\CategoryRepository;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class ProductImageController extends AbstractController
{
    public function __invoke(
        Request $request,
        CategoryRepository $categoryRepository,
        PromotionRepository $promotionRepository
    ) {
        $product = new Product();

        $product->setLabel($request->request->get('label'));

        if ($request->request->get('description')) {
            $product->setDescription($request->request->get('description'));
        }

        $product->setPrice((float) $request->request->get('price'));

        $file = $request->files->get('image');
        if ($file) {
            $product->setFile($file);
        }

        if ($request->request->get('category')) {
            $category = $categoryRepository->find((int) $request->request->get('category'));
            $product->setCategory($category);
        }

        if ($request->request->get('promotion')) {
            $promotion = new Promotion();
            $promotion->setPercentage((float) $request->request->get('promotion'));
            $promotion->setStartDate(new \DateTime($request->request->get('promotionStart')));
            $promotion->setEndDate(new \DateTime($request->request->get('promotionEnd')));
            $product->setPromotion($promotion);
        }

        return $product;
    }
}
