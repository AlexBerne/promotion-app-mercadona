<?php

namespace App\Controller;

use App\Entity\Promotion;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class ProductPatchController extends AbstractController
{
  public function __invoke(
    Request $request,
    ProductRepository $productRepository,
    CategoryRepository $categoryRepository,
    PromotionRepository $promotionRepository
  ) {
    $productId = $request->attributes->get('id');
    $product = $productRepository->find($productId);
    $parameters = json_decode($request->getContent(), true);

    if (!$product) {
      throw $this->createNotFoundException('Product not found');
    }

    if (isset($parameters['label'])) {
      $product->setLabel($parameters['label']);
    }

    if (isset($parameters['price'])) {
      $product->setPrice($parameters['price']);
    }

    if (isset($parameters['description'])) {
      $product->setDescription($parameters['description']);
    }

    if (isset($parameters['category'])) {
      $category = $categoryRepository->find((int) $parameters['category']);
      $product->setCategory($category);
    }

    if ($request->request->get('promotion')) {
      $promotion = new Promotion();
      $promotion->setPercentage((float) $request->request->get('promotion'));
      $promotion->setStartDate(new \DateTime($request->request->get('promotionStart')));
      $promotion->setEndDate(new \DateTime($request->request->get('promotionEnd')));
      $product->setPromotion($promotion);
    }

    return $product;
  }
}
