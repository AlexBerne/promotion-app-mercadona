<div id="top"></div>

<div align="center">
  <h1 align="center">Mercadona Application de Promotions</h1>

  <p align="center">
    <p>
        Projet web pour la gestion des promotions des produits Mercadona.
    </p>
    <a href="http://mercadona.alexandre.berne.tech:3000/">Lien vers la production</a> - 
    <a href="https://trello.com/invite/b/eqSrs1tC/ATTIa1ec5ef77c17131d3dd7fa9474313ab0CAE7DCA6/exam-bloc-3">Lien du Trello</a>
  </p>
</div>

<!-- Sommaire -->
<p>
  <details>
    <summary style="cursor: pointer">Sommaire</summary>
    <ol>
      <li>
        <a href="#à-propos">À-propos ?</a>
        <ul>
          <li><a href="#technologies">Technologies</a></li>
        </ul>
      </li>
      <li>
        <a href="#pour-commencer">Pour commencer</a>
        <ul>
          <li><a href="#prérequis">Prérequis</a></li>
          <li><a href="#installation">Installation</a></li>
        </ul>
      </li>
      <li>
        <a href="#déploiement">Déploiement</a>
      </li>
      <li><a href="#utilisation">Utilisation</a></li>
    </ol>
  </details>
</p>

# À-propos

[![Sceenshot de l'application][product-screenshot]](./documentation/app-homepage.png)

Cette application à pour objectif de proposer à un administrateur l'ajout la supression et la modification de produits ainsi que de catégories. Il est également possible d'ajouter des promotions à ces produits.

<p align="right">(<a href="#top">Retour en haut</a>)</p>

## Technologies

Technologies utilisées pour la réalisation de ce projet et explication des choix techniques.

Backend :

- [Symfony 6](https://symfony.com/) : Demande client. Framework PHP.
- [Doctrine ORM](https://symfony.com/doc/current/doctrine.html) : Object Relational Mapping (ORM) inclus par défaut dans Symfony. Permet de réaliser des scripts sql à partir de code PHP. Inclus également des migrations et des seeders.
- [API Platform](https://api-platform.com/) : Framework supplémentaire à Symfony afin de réaliser une API. Il est possible de le faire sans ce framework mais API Platform simplifie grandement la tâche en plus d'être maintenu et utilisé par une large communauté.
- [Lexik JWT Bundle](https://github.com/lexik/LexikJWTAuthenticationBundle) : Package permettant la gestion de l'authentification stateless au sein de l'application sous la forme d'un token JWT. L'authentification de l'utilisateur se fait par la suite sous la forme d'un Bearer token. Il serait toutefois nécessaire de faire évoluer cette partie (inscription, mot de passe oublié, changement de mot de passe et refresh token).
- [Vich Image Uploader](https://github.com/dustin10/VichUploaderBundle) : Gestion d'upload d'images au sein de Symfony.

Frontend :

- [VueJS 3](https://vuejs.org/) : Framework frontend depuis 2015, VueJS s'est imposé dans le marché est est aujourd'hui une étoile montante utilisée par de nombreux développeurs et entreprises hautement professionnelles telles que (Gitlab, Laravel, Alibaba ou même Nintendo).
- [NuxtJS 3](https://nuxt.com/) : Framework supplémentaire à Vuejs afin de permettre une expérience développeur accrue, des performances améliorées (SSR et SSG) ainsi qu'une gestion de sécurité supplémentaire
- [Vuetify](https://vuetifyjs.com/en/) : Framework UI complet afin de développer le plus rapidement une application Web.
- [Vuelidate](https://vuelidate-next.netlify.app/) : Gestion des formulaires de l'application afin d'envoyer au back les informations nécessaires avec une pré-validation des données.
- [Typescript](https://www.typescriptlang.org/) : Version fortement typée de JavaScript, Typescript s'impose depuis des années dans le développement NodeJS et Frontend.

Base de données :

- [Postgresql](https://www.postgresql.org/) : Gestionnaire de Base de Données.

Déploiement :

- [Docker & docker compose](https://www.docker.com/) : Permet de déploiement et la livraison de l'application sur toutes les machines possédent docker et docker compose. L'utilisation de Kubernetes et le déploiement dans des EKS de Amazon Web Service (par exemple) serait une évolution possible. Il faudrait pour cela réaliser une étude d'impact sur la charge utilisateur de l'application et scaler l'application suivant ces résultats.
- [PHP FPM](https://www.php.net/manual/fr/install.fpm.php) : Server Application Programming Interface permettant la communication entre un serveur web et PHP. Il constitue ainsi une alternative au serveur PHP avec des options pour les sites subissant de fortes charges.
- [Nginx](https://www.nginx.com/) : Serveur web et wrapper à nos services (PostgreSQL, PHP FPM et NuxtJS)

<p align="right">(<a href="#top">Retour en haut</a>)</p>

# Pour commencer

Gestion des dossiers du projet :

- ./documentation : Lié à la documentation de ce README.
- ./frontend : Application NuxtJS dédiée au Frontend.
- ./symfony : Application Symfony dédiée au Backend.
- ./logs : Log du serveur Nginx (non versionné).
- ./nginx : Configuration et Dockerfile dédié à Nginx.
- ./php-fpm : Configuration et Dockerfile dédié à PHP FPM.

<p align="right">(<a href="#top">Retour en haut</a>)</p>

## Prérequis

Pour installer cette application vous avez besoin de Docker et docker-compose. Ainsi que de NodeJS 18+ et npm 9+. Tout le reste est administré par le docker pour le développement. Pour la production, il faut décomenter la partie NuxtJS dans le docker-compose.yml.

- Installation de npm :
  ```sh
  npm install npm@latest -g
  ```

<p align="right">(<a href="#top">Retour en haut</a>)</p>

## Installation

_Pour installer ce projet, vous devez suivre les instructions suivantes :_

1. Cloner le dépot Gitlab
   ```sh
   git clone git@gitlab.com:AlexBerne/promotion-app-mercadona.git
   ```
2. Installer les paquets npm dans le dossier frontend
   ```sh
   npm install
3. Créer un ./frontend/.env à partir du ./frontend/.env.dist
2. Lancer la commande suivante pour lancer le projet en développement
   ```sh
   npm run dev
3. Créer un ./.env à partir du ./.env.dist
3. Créer un ./symfony/.env à partir du ./symfony/.env.dist

<p align="right">(<a href="#top">Retour en haut</a>)</p>

# Deployment

Le déploiement s'effectue avec docker et docker compose. Lancez la commande suivante à la racine du projet :

```bash
docker compose up --build
```

<p align="right">(<a href="#top">Retour en haut</a>)</p>

# Utilisation

**Utilisateur non connecté :**

Un utilisateur non connecté peut accéder à la page d'accueil et visualiser les produits.

**Utilisateur connecté :**

Un utilisateur connecté peut accéder à la page /admin et ajouter, modifier et supprimer des produits.

En cliquant sur l'onglet "Catégories", il peut ajouter, modifier et supprimer des catégories pour ensuite les assigner à un produit.


<p align="right">(<a href="#top">Retour en haut</a>)</p>
