export default defineNuxtConfig({
  modules: ['@invictus.codes/nuxt-vuetify', '@vueuse/nuxt'],

  ssr: false,

  runtimeConfig: {
    public: {
      backendUrl: process.env.BACKEND_URL,
    }
  }
});
