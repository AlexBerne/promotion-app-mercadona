export interface Product {
  id: number;
  label: string;
  description?: string;
  price: number;
  category?: string;
  image?: string;
  promotion?: {
    id: number;
    percentage: number;
    start_date: Date;
    end_date: Date;
  };
}

const products = ref<Product[]>([]);

export const useProduct = () => {
  const config = useRuntimeConfig();

  const fetchProducts = async (refresh = false): Promise<Product[]> => {
    if (products.value.length && !refresh) {
      return products.value;
    }

    const res = await useFetch<Product[]>(`${config.public.backendUrl}api/products`);

    return (products.value = res.data.value ?? []);
  };

  const isFetchingDelete = ref<boolean>(false);

  const deleteProduct = async (id: number): Promise<void> => {
    isFetchingDelete.value = true;
    await useApi<Product[]>(`products/${id}`, {
      method: 'DELETE',
    });
    products.value = products.value.filter((product) => product.id !== id);
    isFetchingDelete.value = false;
  };

  return { products, fetchProducts, deleteProduct, isFetchingDelete };
};
