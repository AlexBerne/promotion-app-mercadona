const title = ref<string>('');

export const useTitle = () => {
  return { title };
};
