export interface User {
  id: number;
  email: string;
  role: string[];
}

export const useAuth = () => {
  const config = useRuntimeConfig();
  const router = useRouter();
  const token = useLocalStorage('token', '');
  const user = useLocalStorage<User>('user', {} as User);

  const fetchToken = async (username: string, password: string) => {
    const res = await $fetch<{ token: string }>(
      `${config.public.backendUrl}api/login_check`,
      {
        method: 'POST',
        body: { username, password },
      },
    );

    token.value = res.token;
  };

  const fetchUser = async () => {
    const res = await useApi<User>('me');

    user.value = res;
  };

  const logout = () => {
    token.value = '';
    user.value = {} as User;
    router.push('/auth/login');
  };

  const isUserLogged = computed<boolean>(
    () => Object.keys(user.value).length > 0,
  );

  return { token, user, fetchToken, fetchUser, logout, isUserLogged };
};
