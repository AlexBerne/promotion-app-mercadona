import { NitroFetchRequest, NitroFetchOptions } from 'nitropack';

/**
 * Composable to use the NitroPack fetcher with the auth Bearer token in the headers
 */
export const useApi = <T = unknown>(
  request: NitroFetchRequest,
  opts?: NitroFetchOptions<any, any>,
) => {
  const { token, logout } = useAuth();
  const config = useRuntimeConfig();

  return $fetch<T>(config.public.backendUrl + 'api/' + request, {
    ...opts,
    headers: {
      Accept: 'application/json',
      ...opts?.headers,
      Authorization: 'Bearer ' + token.value,
    },
  }).catch((err) => {
    logout();
    throw err;
  });
};
