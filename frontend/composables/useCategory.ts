export interface Category {
  id: number;
  label: string;
}

const categories = ref<Category[]>([]);

export const useCategory = () => {
  const config = useRuntimeConfig();
  
  const fetchCategories = async (): Promise<Category[]> => {
    const res = await useFetch<Category[]>(
      `${config.public.backendUrl}api/categories`,
    );

    return (categories.value = res.data.value ?? []);
  };

  const findCategoryByUrl = (url: string): Category | undefined => {
    const categoryId = +url.replace('/api/categories/', '');
    if (!categoryId) {
      return undefined
    }
    return categories.value.find((c) => c.id === categoryId);
  };
  return { categories, fetchCategories, findCategoryByUrl };
};
